<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \MikeFrancis\LaravelUnleash\Unleash;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function () {
    return 'hello world !';
});

Route::get('/test-unleash', function () {

    $unleash = app(Unleash::class);

    return [
        'test-flagging-enabled' => $unleash->isFeatureEnabled('test-flagging'),
    ];
});

Route::get('/unleash-features', function () {

    $unleash = app(Unleash::class);

    return $unleash->getFeatures();
});
