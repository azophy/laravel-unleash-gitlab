# Experiment on using Gitlab's Feature Flagging with Laravel

See route/api.php for examples

## Running this repo

- clone this repo
- edit `.env.example` into `.env`
- `composer install`
- `php artisan serve`

## Testing feature toggle
- create new flag named `test-flagging` in `https://<url to gitlab repo>/-/feature_flags/new`
- fill .env accordingly
- try hitting `localhost:8000/api/test-unleash` or `localhost:8000/api/unleash-features`

## Gotchas
- the "API URL" from gitlab's "Configure" menu need to be adjusted for the Laravel-Unleash package used here:
  - the domain/host & feature endpoint part need to be split, as Laravel-Unleash packge separate those two variables
  - we also need to add "/features" to the end of the url to make it work (found out about it fromi the example here: https://github.com/mikefrancis/laravel-unleash/pull/28#issue-606632979 )

## Software/Library/Package used:
- [Laravel Framework](https://laravel.com/).
- [Gitlab's Feature Flagging](https://docs.gitlab.com/ee/operations/feature_flags.html)
- [Mike Francis Laravel-Unleash package](https://github.com/mikefrancis/laravel-unleash)

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
